//
//  DetailViewController.m
//  SuperScore
//
//  Created by willshere on 12/9/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import "DetailViewController.h"

#import "TopPlayerObject.h"
#import "TeamPlayerObject.h"

@interface DetailViewController ()

    @property (strong, nonatomic) IBOutlet UILabel *playerNameLabel;
@end

@implementation DetailViewController

+(DetailViewController *)detailViewController
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
    return [story instantiateViewControllerWithIdentifier:@"DetailViewControllerID"];
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI ];
    // Do any additional setup after loading the view.
}
    
-(void)setupUI
{
    if (self.type == kDetailTypeTopPlayer)
    {
        [self.playerNameLabel setText:self.goalsObject.goalsObjectPersonName];
    }
    else if (self.type == kDetailTypeTeamPlayer)
    {
        [self.playerNameLabel setText:self.peopleObject.peopleObjectName];
    }
    else
    {
        [self.playerNameLabel setText:@""];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - IBAction
    
- (IBAction)backDidSelect:(id)sender
    {
        [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
