//
//  DetailViewController.h
//  SuperScore
//
//  Created by willshere on 12/9/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    kDetailTypeTopPlayer = 0,
    kDetailTypeTeamPlayer,
    
}DetailType;


@class PeopleObject;
@class GoalsObject;


@interface DetailViewController : UIViewController

+(DetailViewController *)detailViewController;

    @property (nonatomic) DetailType type;
    @property (strong, nonatomic) GoalsObject *goalsObject;
    @property (strong, nonatomic) PeopleObject *peopleObject;
    
@end
