//
//  TeamPlayerTableViewController.h
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PeopleObject;

@protocol TeamPlayerTableViewControllerDelegate <NSObject>
    
-(void)TeamPlayerTableViewDidSelect:(PeopleObject *)peopleObject;
    
@end

@interface TeamPlayerTableViewController : UITableViewController

@property (weak, nonatomic) id<TeamPlayerTableViewControllerDelegate> delegate;
    
+(TeamPlayerTableViewController *)teamPlayerTableViewController;
    
@end
