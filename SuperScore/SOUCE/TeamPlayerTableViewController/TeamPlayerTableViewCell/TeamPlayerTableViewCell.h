//
//  TeamPlayerTableViewCell.h
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PeopleObject;

@interface TeamPlayerTableViewCell : UITableViewCell

-(void)settingTeamPlayerTableViewCellWithPeopleObject:(PeopleObject *)peopleObject;
    
@end
