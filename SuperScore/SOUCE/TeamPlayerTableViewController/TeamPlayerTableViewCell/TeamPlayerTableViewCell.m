//
//  TeamPlayerTableViewCell.m
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import "TeamPlayerTableViewCell.h"

#import "TeamPlayerObject.h"

@interface TeamPlayerTableViewCell ()
    
    @property (weak, nonatomic) IBOutlet UILabel *playerNameLabel;
    @property (weak, nonatomic) IBOutlet UILabel *playerNumberLabel;
    
@end

@implementation TeamPlayerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
    
-(void)settingTeamPlayerTableViewCellWithPeopleObject:(PeopleObject *)peopleObject
{
    NSLog(@"pro pro : %@ ",peopleObject.peopleObjectName);
    if (peopleObject.peopleObjectShirtNumber > 0)
    {
       [self.playerNumberLabel setText:[NSString stringWithFormat:@"#%d",(int)peopleObject.peopleObjectShirtNumber]];
    }
    else
    {
       [self.playerNumberLabel setText:[NSString stringWithFormat:@""]];
    }
        [self.playerNameLabel setText:[NSString stringWithFormat:@"%@",peopleObject.peopleObjectName]];
   
}

@end
