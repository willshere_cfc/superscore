//
//  TeamPlayerTableViewController.m
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import "TeamPlayerTableViewController.h"

#import "TeamPlayerTableViewCell.h"

#import "ConfigRes.h"
#import "WMAFNetWorking.h"

#import "TeamPlayerObject.h"

@interface TeamPlayerTableViewController () 
    
    @property (nonatomic, strong) NSArray *teamPlayerArray;
    
    @end

@implementation TeamPlayerTableViewController
    
+(TeamPlayerTableViewController *)teamPlayerTableViewController
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        return [story instantiateViewControllerWithIdentifier:@"TeamPlayerTableViewControllerID"];
    }
    
    static NSString * const reuseIdentifier = @"Cell";
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requestService ];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
    
-(void)requestService
    {
        [ConfigRes loadingView];
        
        [WMAFNetWorking GET:[NSString stringWithFormat:@"%@:443/teams/662/players",ROOT_URL]
                  parameter:nil
     acceptableContentTypes:nil
                  withBlock:^(id value, NSError *error) {
                      
                      NSLog(@"value : %@ ",value);
                      
                      if (!error)
                      {
                          self.teamPlayerArray =  [TeamPlayerObject arrayTeamPlayerObjectWithAttributes:[value valueForKey:@"players"]];
                          
                          
                          
                          [self.tableView reloadData];
                          
                          [ConfigRes unloadingView];
                      }
                      else
                      {
                          [ConfigRes unloadingView];
                          
                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                                                          message:nil
                                                                         delegate:self
                                                                cancelButtonTitle:@"Close"
                                                                otherButtonTitles:nil];
                          [alert show];
                      }
                  }];
    }
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - Table view data source
    
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [self.teamPlayerArray count];
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    TeamPlayerObject *topPlayerObject = [self.teamPlayerArray objectAtIndex:section];
    return [topPlayerObject.teamPlayerObjectPeopleArray count];
}
    
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
    
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 20)];
        [sectionView setBackgroundColor:[UIColor lightGrayColor]];
        
        
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((10.0f ) + 10.0f, 11.0f, 200.0, 20.0f)];
        [titleLabel setBackgroundColor:[UIColor lightGrayColor]];
        [titleLabel setTextColor:[UIColor blackColor]];
        [titleLabel setFont:[UIFont systemFontOfSize:20]];;
        
        TeamPlayerObject *topPlayerObject = [self.teamPlayerArray objectAtIndex:section];
        
        [titleLabel setText:topPlayerObject.teamPlayerObjectPositionName];
        [sectionView addSubview:titleLabel];
        
        
        
        return sectionView;
}
    
    
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    TeamPlayerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    TeamPlayerObject *topPlayerObject = [self.teamPlayerArray objectAtIndex:indexPath.section];
    PeopleObject *peopleObject = [topPlayerObject.teamPlayerObjectPeopleArray objectAtIndex:indexPath.row];
    
    [cell settingTeamPlayerTableViewCellWithPeopleObject:peopleObject];
    
    return cell;
}
    
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
         [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        TeamPlayerObject *topPlayerObject = [self.teamPlayerArray objectAtIndex:indexPath.section];
        PeopleObject *peopleObject = [topPlayerObject.teamPlayerObjectPeopleArray objectAtIndex:indexPath.row];
        
        [self.delegate TeamPlayerTableViewDidSelect:peopleObject];
    }
    

@end
