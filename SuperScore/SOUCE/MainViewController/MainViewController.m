//
//  MainViewController.m
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import "MainViewController.h"

#import "CAPSPageMenu.h"

#import "TopPlayerTableViewController.h"
#import "TeamPlayerTableViewController.h"

#import "DetailViewController.h"

@interface MainViewController () <CAPSPageMenuDelegate , TeamPlayerTableViewControllerDelegate , TopPlayerTableViewControllerDelegate>

    @property (nonatomic) CAPSPageMenu *pageMenu;
    
@end

@implementation MainViewController

+(MainViewController *)mainViewController
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
    return [story instantiateViewControllerWithIdentifier:@"MainViewControllerID"];
}
    
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self setupPageSlider];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - CAPSPAGE DELEGATE
    
- (void)didTapGoToLeft {
    NSInteger currentIndex = self.pageMenu.currentPageIndex;
    
    if (currentIndex > 0) {
        [_pageMenu moveToPage:currentIndex - 1];
    }
}
    
- (void)didTapGoToRight {
    NSInteger currentIndex = self.pageMenu.currentPageIndex;
    
    if (currentIndex < self.pageMenu.controllerArray.count) {
        [self.pageMenu moveToPage:currentIndex + 1];
    }
}

#pragma mark - SETUP SLIDER
    
-(void)setupPageSlider
    {
        TopPlayerTableViewController *topPlayerTableViewController = [TopPlayerTableViewController topPlayerTableViewController];
        [topPlayerTableViewController setDelegate:self ];
        topPlayerTableViewController.title = @"Top Player";
        
        TeamPlayerTableViewController *teamPlayerTableViewController = [TeamPlayerTableViewController teamPlayerTableViewController];
        [teamPlayerTableViewController setDelegate:self ];
        teamPlayerTableViewController.title = @"Player";
        
        
        NSArray *controllerArray = @[topPlayerTableViewController, teamPlayerTableViewController];
        NSDictionary *parameters = @{
                                     CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:30.0/255.0 alpha:1.0],
                                     CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                     CAPSPageMenuOptionSelectionIndicatorColor: [UIColor colorWithRed:204.0/255.0 green:209.0/255.0 blue:118.0/255.0 alpha:1.0],
                                     CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                     CAPSPageMenuOptionMenuItemFont: [UIFont systemFontOfSize:20],
                                     CAPSPageMenuOptionSelectedMenuItemLabelColor: [UIColor colorWithRed:204.0/255.0 green:209.0/255.0 blue:118.0/255.0 alpha:1.0],
                                     CAPSPageMenuOptionMenuHeight: @(40.0),
                                     CAPSPageMenuOptionMenuItemWidth: @(160.0),
                                     CAPSPageMenuOptionCenterMenuItems: @(YES)
                                     };
        
        _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 64.0, self.view.frame.size.width, self.view.frame.size.height -64) options:parameters];
        [_pageMenu setDelegate:self];
        [self.view addSubview:_pageMenu.view];
    }


#pragma mark - TeamPlayerTableViewController Delegate
    
-(void)TeamPlayerTableViewDidSelect:(PeopleObject *)peopleObject
{
    DetailViewController *detailViewController = [DetailViewController detailViewController];
    [detailViewController setType:kDetailTypeTeamPlayer];
    detailViewController.peopleObject = peopleObject;
    
    [self.navigationController pushViewController:detailViewController animated:YES];
}
    
#pragma mark - TopPlayerTableViewController Delegate

-(void)topPlayerTableViewDidSelect:(GoalsObject *)goalObject
{
    DetailViewController *detailViewController = [DetailViewController detailViewController];
    [detailViewController setType:kDetailTypeTopPlayer];
    detailViewController.goalsObject = goalObject;
    
     [self.navigationController pushViewController:detailViewController animated:YES];
}
    
@end
