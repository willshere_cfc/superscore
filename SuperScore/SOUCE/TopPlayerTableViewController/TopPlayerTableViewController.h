//
//  TopPlayerTableViewController.h
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoalsObject;

@protocol TopPlayerTableViewControllerDelegate <NSObject>
    
-(void)topPlayerTableViewDidSelect:(GoalsObject *)goalObject;
    
@end

@interface TopPlayerTableViewController : UITableViewController
    
@property (weak, nonatomic) id<TopPlayerTableViewControllerDelegate> delegate;

+(TopPlayerTableViewController *)topPlayerTableViewController;
    
@end
