//
//  TopPlayerTableViewCell.m
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import "TopPlayerTableViewCell.h"

#import "TopPlayerObject.h"

@interface TopPlayerTableViewCell ()
    
    @property (weak, nonatomic) IBOutlet UILabel *playerNameLabel;
    @property (weak, nonatomic) IBOutlet UILabel *playerGoalsLabel;
    
@end

@implementation TopPlayerTableViewCell
    
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
    
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
    
-(void)settingTopPLayerTableViewCellWithGoalsObject:(GoalsObject *)goalsObject
{
        NSLog(@"goal Obj : %@ ",goalsObject);
        
        [self.playerNameLabel setText:[NSString stringWithFormat:@"%@",goalsObject.goalsObjectPersonName]];
        [self.playerGoalsLabel setText:[NSString stringWithFormat:@"%d",(int)goalsObject.goalsObjectCountGoal]];
}
    
    @end
