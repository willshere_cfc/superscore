//
//  TopPlayerTableViewController.m
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import "TopPlayerTableViewController.h"

#import "TopPlayerTableViewCell.h"

#import "ConfigRes.h"
#import "WMAFNetWorking.h"

#import "TopPlayerObject.h"

@interface TopPlayerTableViewController ()
    
    @property (nonatomic, strong) NSArray *topPlayerArray;
    
@end

@implementation TopPlayerTableViewController
    
+(TopPlayerTableViewController *)topPlayerTableViewController
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        return [story instantiateViewControllerWithIdentifier:@"TopPlayerTableViewControllerID"];
    }
    
    static NSString * const reuseIdentifier = @"Cell";
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requestService];

}
    
-(void)requestService
    {
        [ConfigRes loadingView];
        
        [WMAFNetWorking GET:[NSString stringWithFormat:@"%@/teams/662/topPlayers?seasonId12653",ROOT_URL]
                  parameter:nil
     acceptableContentTypes:nil
                  withBlock:^(id value, NSError *error) {
                      
                      NSLog(@"value : %@ ",value);
                      
                      if (!error)
                      {
                          self.topPlayerArray =  [TopPlayerObject arrayTopPlayerObjectWithAttributes:[value valueForKey:@"players"]];
              
                          [self.tableView reloadData];
                          
                          [ConfigRes unloadingView];
                      }
                      else
                      {
                          [ConfigRes unloadingView];
                          
                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                                                          message:nil
                                                                         delegate:self
                                                                cancelButtonTitle:@"Close"
                                                                otherButtonTitles:nil];
                          [alert show];
                      }
                  }];
    }
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - Table view data source
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    TopPlayerObject *topPlayerObject = [self.topPlayerArray objectAtIndex:0];
    SeasonObject *seasonObject = [topPlayerObject.topPlayerObjectSeasonsArray objectAtIndex:0];
    
    return seasonObject.seasonObjectGoalsArray.count;
}
    
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    TopPlayerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    TopPlayerObject *topPlayerObject = [self.topPlayerArray objectAtIndex:0];
    SeasonObject *seasonObject = [topPlayerObject.topPlayerObjectSeasonsArray objectAtIndex:0];
    GoalsObject *goalsObject = [seasonObject.seasonObjectGoalsArray objectAtIndex:indexPath.row];

    [cell settingTopPLayerTableViewCellWithGoalsObject:goalsObject];
    
    return cell;
}
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        TopPlayerObject *topPlayerObject = [self.topPlayerArray objectAtIndex:0];
        SeasonObject *seasonObject = [topPlayerObject.topPlayerObjectSeasonsArray objectAtIndex:0];
        GoalsObject *goalsObject = [seasonObject.seasonObjectGoalsArray objectAtIndex:indexPath.row];
        
        [self.delegate topPlayerTableViewDidSelect:goalsObject];
    }
    
@end
