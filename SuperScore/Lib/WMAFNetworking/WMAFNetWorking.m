//
//  WMAFNetWorking.m
//  
//
//  Created by Willshere on 3/30/2558 BE.
//  Copyright (c) 2558 NILECON THAILAND. All rights reserved.
//

#import "WMAFNetWorking.h"
#import "AFHTTPRequestOperationManager.h"
//#import <SHXMLParser/SHXMLParser.h>


@implementation WMAFNetWorking

+(void)POST:(NSString *)url parameter:(NSDictionary *)parameter acceptableContentTypes:(NSString *)contentTypes withBlock:(void (^)(id value, NSError *error))block
{
    NSLog(@"url :: %@",url);
    if (contentTypes == nil)
        contentTypes = @"application/json";
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:contentTypes];
    operationManager.responseSerializer = [AFJSONResponseSerializer serializer];
    operationManager.securityPolicy = securityPolicy;
    
    [operationManager POST:url parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (responseObject == nil || [responseObject count] == 0)
         {
             NSLog(@"No Response");
             block([NSDictionary dictionary], nil);
         }
         else
         {
             if (block)
             {
                 block(responseObject, nil);
             }
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         NSLog(@"Error: %@", error);
         if (block) {
             block([NSDictionary dictionary], error);
         }
     }];
}

+(void)GET:(NSString *)url parameter:(NSDictionary *)parameter acceptableContentTypes:(NSString *)contentTypes withBlock:(void (^)(id value, NSError *error))block
{
    if (contentTypes == nil)
        contentTypes = @"application/json";
    
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    operationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:contentTypes];
    
    [operationManager GET:url parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (responseObject == nil || [responseObject count] == 0)
            NSLog(@"No Response");
        else
        {
            if (block)
            {
                block(responseObject, nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        if (block) {
            block([NSDictionary dictionary], error);
        }
    }];
}

+(void)POST_MultiPartFormData:(NSString *)url
                    parameter:(NSDictionary *)parameter
       acceptableContentTypes:(NSString *)contentTypes
                     fileDatas:(NSArray *)files
                         name:(NSString *)name
                     fileName:(NSString *)fileName
                     mimeType:(NSString *)mimeType
                    withBlock:(void (^)(id value, NSError *error))block
{
    if (contentTypes == nil)
        contentTypes = @"application/json";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:contentTypes, nil];
    [manager.requestSerializer setTimeoutInterval:120];// Time out after 120 seconds
    
    [manager POST:url parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         int i = 1;
         NSString *fileNameString = [[fileName componentsSeparatedByString:@"."] objectAtIndex:0];
         NSString *pathExtention = [fileName pathExtension];
         
         NSLog(@"fileName :: %@",fileNameString);
         
         for (NSData *file in files)
         {
             [formData appendPartWithFileData:file
                                         name:[NSString stringWithFormat:@"%@%d",name,i]
                                     fileName:[NSString stringWithFormat:@"%@%d.%@",fileNameString,i,pathExtention]
                                     mimeType:mimeType];
             i++;
         }
         
     } success:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         if (responseObject == nil || [responseObject count] == 0)
             NSLog(@"No Response");
         else
         {
             if (block)
             {
                 block(responseObject, nil);
             }
         }
          
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         if (block)
         {
             block([NSDictionary dictionary], error);
         }
     }];
}

+(id)GET_Syncronyc:(NSString *)url parameter:(NSDictionary *)parameter acceptableContentTypes:(NSString *)contentTypes
{
    if (contentTypes == nil)
        contentTypes = @"application/json";
    
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:contentTypes];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation start];
    [operation waitUntilFinished];
    
    return [operation responseObject];
}

+(void)startMotitorConnection
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

+(void)stoptMotitorConnection
{
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

+(BOOL)checkConnectIntenet
{
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

/*
+(void)XMLRequest:(NSString *)url withBlock:(void (^)(id value, NSError *error))block
{
    NSURL *apiUrl = [NSURL URLWithString:url];
    NSData *data = [NSData dataWithContentsOfURL:apiUrl];
    
    SHXMLParser *parser = [[SHXMLParser alloc] init];
    id responseObject = [parser parseData:data];
    
    if (responseObject == nil || [responseObject count] == 0)
        NSLog(@"No Response");
    else
    {
        if (block)
        {
            block(responseObject, nil);
        }
        else
        {
           
        }
    }
}*/

@end
