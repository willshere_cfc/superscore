//
//  WMAFNetWorking.h
//  
//
//  Created by Willshere on 3/30/2558 BE.
//  Copyright (c) 2558 NILECON THAILAND. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WMAFNetWorking : NSObject

+(void)POST:(NSString *)url parameter:(NSMutableDictionary *)parameter acceptableContentTypes:(NSString *)contentTypes withBlock:(void (^)(id value, NSError *error))block;

+(void)GET:(NSString *)url parameter:(NSDictionary *)parameter acceptableContentTypes:(NSString *)contentTypes withBlock:(void (^)(id value, NSError *error))block;

+(id)GET_Syncronyc:(NSString *)url parameter:(NSDictionary *)parameter acceptableContentTypes:(NSString *)contentTypes;

+(void)POST_MultiPartFormData:(NSString *)url
                    parameter:(NSDictionary *)parameter
       acceptableContentTypes:(NSString *)contentTypes
                    fileDatas:(NSArray *)files
                         name:(NSString *)name
                     fileName:(NSString *)fileName
                     mimeType:(NSString *)mimeType
                    withBlock:(void (^)(id value, NSError *error))block;

+(void)startMotitorConnection;
+(void)stoptMotitorConnection;
+(BOOL)checkConnectIntenet;
//+(void)XMLRequest:(NSString *)url withBlock:(void (^)(id value, NSError *error))block;

@end
