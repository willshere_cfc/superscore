



#import "ConfigRes.h"

#import "MRProgressOverlayView.h"

#import "AppDelegate.h"

//#import "MMProgressHUD+Animations.h"

#pragma mark Font
NSString *const FONT_MONTSERRAT_REGULAR = @"Montserrat-Regular";
NSString *const FONT_OLEOSCRIPT_BOLD = @"OleoScript-Bold";
NSString *const FONT_OLEOSCRIPT_REGULAR = @"oleoscript-regular-webfont";
NSString *const FONT_Samsung = @"SamsungSharpSans-Medium";

//NSString *const HTTPS_HOST = @"http://trippino.api-nilecon.com/api";
//NSString *const HTTP_HOST = @"http://trippino.api-nilecon.com/api";

@implementation ConfigRes

+(float)floatFormRGBByFloat:(float)val{
    return val/255;
}

+(UIColor*)COLORTEXT_GRAY{
    return [UIColor colorWithRed:[ConfigRes floatFormRGBByFloat:139.0f] green:[ConfigRes floatFormRGBByFloat:146.0f] blue:[ConfigRes floatFormRGBByFloat:150.0f] alpha:1.0f];
}


+(UIColor*)COLORTEXT_BLUE{
    return [UIColor colorWithRed:[ConfigRes floatFormRGBByFloat:0.0f] green:[ConfigRes floatFormRGBByFloat:145.0f] blue:[ConfigRes floatFormRGBByFloat:210.0f] alpha:1.0f];
    
}

+(UIColor*)COLORTEXT_RED{
    return [UIColor colorWithRed:[ConfigRes floatFormRGBByFloat:222.0f] green:[ConfigRes floatFormRGBByFloat:0.0f] blue:[ConfigRes floatFormRGBByFloat:5.0f] alpha:1.0f];
    
}

+(UIColor*)COLORTEXT_YELLOW{
    return [UIColor colorWithRed:[ConfigRes floatFormRGBByFloat:255.0f] green:[ConfigRes floatFormRGBByFloat:255.0f] blue:[ConfigRes floatFormRGBByFloat:1.0f] alpha:1.0f];
    
}

+(UIColor*)COLORTEXT_ORANGE{
    return [UIColor colorWithRed:[ConfigRes floatFormRGBByFloat:255.0f] green:[ConfigRes floatFormRGBByFloat:51.0f] blue:[ConfigRes floatFormRGBByFloat:51.0f] alpha:1.0f];
    
}

+(UIColor*)COLORTEXT_PINK{
    return [UIColor colorWithRed:[ConfigRes floatFormRGBByFloat:255.0f] green:[ConfigRes floatFormRGBByFloat:0.0f] blue:[ConfigRes floatFormRGBByFloat:153.0f] alpha:1.0f];
    
}

+ (id)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    
    activityController.excludedActivityTypes = @[UIActivityTypeAirDrop,
                                                UIActivityTypePostToFacebook];
    
//    [activityController setCompletionHandler:^(NSString *activityType, BOOL completed) {
//        if (!completed) return;
//        
//        NSLog(@"Share Completion");
//   
//    }];
    
    return activityController;
    // [self presentViewController:activityController animated:YES completion:nil];
}

+ (BOOL)validEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)validEngAndNumber:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z.-]";
    NSString *laxString = @"[A-Z0-9a-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSLog(@"emailRegex :: %@",emailRegex);
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - MRProgress

static MRProgressOverlayView *progress_mr;



+ (void)loadingView {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    progress_mr = [MRProgressOverlayView new];
    [progress_mr setMode:MRProgressOverlayViewModeIndeterminateSmall];
//    progress_mr setTintColor:<#(UIColor *)#>
    [appDelegate.window addSubview:progress_mr];
    [progress_mr show:YES];
    
}

+ (void)unloadingView {
    
    [progress_mr dismiss:YES];
    
}

@end
