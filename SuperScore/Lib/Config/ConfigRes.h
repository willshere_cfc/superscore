




#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define ROOT_URL  @"https://api-test.superscores.com"



#define IS_IPHONE ( [[[UIDevice currentDevice] model] isEqualToString:@"iPhone"] )
#define IS_IPOD   ( [[[UIDevice currentDevice ] model] isEqualToString:@"iPod touch"] )
#define IS_IPAD   ( [[[UIDevice currentDevice ] model] isEqualToString:@"iPad"] || [[[UIDevice currentDevice ] model] isEqualToString:@"iPad Simulator"] )
#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
#define IS_IPHONE_5 ( IS_IPHONE && IS_HEIGHT_GTE_568 )

#define TABLEVIEW_PAGE_SIZE 20
#define DEFAULT_BEGIN_LOADING 1
#define ITEMS_REUSE_COUNT 50

extern NSString *const FONT_Samsung;
extern NSString *const FONT_MONTSERRAT_REGULAR;
extern NSString *const FONT_OLEOSCRIPT_BOLD;
extern NSString *const FONT_OLEOSCRIPT_REGULAR;

extern NSString *const HTTPS_HOST;
extern NSString *const HTTP_HOST;



@interface ConfigRes : NSObject

+ (void)loadingView ;
+ (void)unloadingView;

+ (id)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url;
+ (BOOL)validEmail:(NSString *)checkString;
+ (BOOL)validEngAndNumber:(NSString *)checkString;


@end
