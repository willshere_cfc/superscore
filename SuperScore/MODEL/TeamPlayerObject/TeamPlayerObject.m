//
//  TeamPlayerObject.m
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import "TeamPlayerObject.h"

@interface TeamPlayerObject()
    
    @property (nonatomic, strong) NSString *teamPlayerObjectPositionName;
    @property (nonatomic, strong) NSArray *teamPlayerObjectPeopleArray;
    
    @end

@implementation TeamPlayerObject
    
+(NSArray *)arrayTeamPlayerObjectWithAttributes:(NSArray *)attributes
    {
        NSMutableArray *array = [NSMutableArray array];
        
        for (NSDictionary *attribute in attributes)
        {
            for (NSDictionary *peopleAttribute in [attribute valueForKey:@"position"])
            {
                TeamPlayerObject *object = [[TeamPlayerObject alloc] initTeamPlayerObjectWithattribute:peopleAttribute];
                [array addObject:object];
            }
            
        }
        
        return [NSArray arrayWithArray:array];
    }
    
-(id)initTeamPlayerObjectWithattribute:(NSDictionary *)attribute
    {
        self = [super init];
        if (!self) {
            return nil;
        }
        
        self.teamPlayerObjectPositionName = [[attribute valueForKey:@"positionName"] copy];
        
        self.teamPlayerObjectPeopleArray = [PeopleObject arrayPeopleObjectWithAttributes:[attribute valueForKey:@"people"]];
        
        return self;
    }
    
-(id)copy
    {
        TeamPlayerObject *object = [[TeamPlayerObject alloc] init];
        
        object.teamPlayerObjectPositionName = [self.teamPlayerObjectPositionName copy];
        object.teamPlayerObjectPeopleArray = [self.teamPlayerObjectPeopleArray copy];
        
        
        return object;
    }
    
    @end


@interface PeopleObject ()
    
    @property  NSInteger peopleObjectPersonId;
    @property (nonatomic, strong) NSString *peopleObjectName;
    @property  NSInteger peopleObjectShirtNumber;
    @property  NSInteger peopleObjectNationalityId;
    @property (nonatomic, strong) NSArray *peopleObjectStatistics;
    
    @end

@implementation PeopleObject
    
+(NSArray *)arrayPeopleObjectWithAttributes:(NSArray *)attributes
    {
        NSMutableArray *array = [NSMutableArray array];
        
        for (NSDictionary *attribute in attributes)
        {
            
            PeopleObject *object = [[PeopleObject alloc] initPeopleObjectWithattribute:attribute];
            [array addObject:object];
            
            
        }
        
        return [NSArray arrayWithArray:array];
    }
    
-(id)initPeopleObjectWithattribute:(NSDictionary *)attribute
    {
        self = [super init];
        if (!self) {
            return nil;
        }
        
        self.peopleObjectPersonId = [[attribute valueForKey:@"personId"] integerValue];
        self.peopleObjectName = [[attribute valueForKey:@"name"] copy];
        self.peopleObjectShirtNumber = [[attribute valueForKey:@"shirtNumber"] integerValue];
        self.peopleObjectNationalityId = [[attribute valueForKey:@"nationalityId"] integerValue];
        
        
        
        return self;
    }
    
    
-(id)copy
    {
        PeopleObject *object = [[PeopleObject alloc] init];
        
        object.peopleObjectPersonId = self.peopleObjectPersonId;
        object.peopleObjectName = [self.peopleObjectName copy];
        object.peopleObjectShirtNumber = self.peopleObjectShirtNumber;
        object.peopleObjectNationalityId = self.peopleObjectNationalityId;
        
        return object;
    }
    
    @end

