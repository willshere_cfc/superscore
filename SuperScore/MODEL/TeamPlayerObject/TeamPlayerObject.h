//
//  TeamPlayerObject.h
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamPlayerObject : NSObject
    
    @property (nonatomic, readonly) NSString *teamPlayerObjectPositionName;
    @property (nonatomic, readonly) NSArray *teamPlayerObjectPeopleArray;
    
+(NSArray *)arrayTeamPlayerObjectWithAttributes:(NSArray *)attributes;
-(id)initTeamPlayerObjectWithattribute:(NSDictionary *)attribute;
    
    @end


@interface PeopleObject : NSObject
    
    @property (readonly) NSInteger peopleObjectPersonId;
    @property (nonatomic, readonly) NSString *peopleObjectName;
    @property (readonly) NSInteger peopleObjectShirtNumber;
    @property (readonly) NSInteger peopleObjectNationalityId;
    @property (nonatomic, readonly) NSArray *peopleObjectStatistics;
    
+(NSArray *)arrayPeopleObjectWithAttributes:(NSArray *)attributes;
-(id)initPeopleObjectWithattribute:(NSDictionary *)attribute;
    
    @end
