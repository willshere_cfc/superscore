//
//  TopPlayerObject.h
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>

@interface TopPlayerObject : NSObject
    
    @property (readonly) NSInteger topPlayerObjectCompetitionId;
    @property (nonatomic, readonly) NSString *topPlayerObjectCompetitionName;
    @property (nonatomic, readonly) NSArray *topPlayerObjectSeasonsArray;
    
+(NSArray *)arrayTopPlayerObjectWithAttributes:(NSArray *)attributes;
-(id)initTopPlayerObjectWithattribute:(NSDictionary *)attribute;
    
    @end




@interface SeasonObject : NSObject
    
    @property (readonly) NSInteger seasonObjectSeasonId;
    @property (nonatomic, readonly) NSString *seasonObjectSeasonName;
    @property (nonatomic, readonly) NSArray *seasonObjectGoalsArray;
    
+(NSArray *)arraySeasonObjectWithAttributes:(NSArray *)attributes;
-(id)initSeasonObjectWithattribute:(NSDictionary *)attribute;
    
    @end




@interface GoalsObject : NSObject
    
    @property (readonly) NSInteger goalsObjectPersonId;
    @property (readonly) NSInteger goalsObjectCountGoal;
    @property (nonatomic, readonly) NSString *goalsObjectPersonName;
    @property (readonly) NSInteger goalsObjectTeamId;
    @property (nonatomic, readonly) NSString *goalsObjectTeamName;
    
+(NSArray *)arrayGoalsObjectWithAttributes:(NSArray *)attributes;
-(id)initGoalsObjectWithattribute:(NSDictionary *)attribute;
    
    @end

