//
//  TopPlayerObject.m
//  SuperScore
//
//  Created by willshere on 12/8/2559 BE.
//  Copyright © 2559 willshere. All rights reserved.
//

#import "TopPlayerObject.h"

@interface TopPlayerObject()
    
    @property  NSInteger topPlayerObjectCompetitionId;
    @property (nonatomic, strong) NSString *topPlayerObjectCompetitionName;
    @property (nonatomic, strong) NSArray *topPlayerObjectSeasonsArray;

    
@end

@implementation TopPlayerObject
    
+(NSArray *)arrayTopPlayerObjectWithAttributes:(NSArray *)attributes
    {
        NSMutableArray *array = [NSMutableArray array];
        
        for (NSDictionary *attribute in attributes)
        {
            TopPlayerObject *object = [[TopPlayerObject alloc] initTopPlayerObjectWithattribute:attribute];
            [array addObject:object];
        }
        
        return [NSArray arrayWithArray:array];
    }
    
-(id)initTopPlayerObjectWithattribute:(NSDictionary *)attribute
    {
        self = [super init];
        if (!self) {
            return nil;
        }
        
        self.topPlayerObjectCompetitionId = [[attribute valueForKey:@"competitionId"] integerValue];
        self.topPlayerObjectCompetitionName = [[attribute valueForKey:@"competitionName"] copy];
        
        self.topPlayerObjectSeasonsArray = [SeasonObject arraySeasonObjectWithAttributes:[attribute valueForKey:@"seasons"]];
        
        return self;
    }
    
-(id)copy
    {
        TopPlayerObject *object = [[TopPlayerObject alloc] init];
        
        object.topPlayerObjectCompetitionId = self.topPlayerObjectCompetitionId;
        object.topPlayerObjectCompetitionName = [self.topPlayerObjectCompetitionName copy];
        object.topPlayerObjectSeasonsArray = [self.topPlayerObjectSeasonsArray copy];
        
        
        return object;
    }
    
@end

@interface SeasonObject ()
    
    @property  NSInteger seasonObjectSeasonId;
    @property (nonatomic, strong) NSString *seasonObjectSeasonName;
    @property (nonatomic, strong) NSArray *seasonObjectGoalsArray;

    
@end

@implementation SeasonObject
    
+(NSArray *)arraySeasonObjectWithAttributes:(NSArray *)attributes
    {
        NSMutableArray *array = [NSMutableArray array];
        
        for (NSDictionary *attribute in attributes)
        {
            SeasonObject *object = [[SeasonObject alloc] initSeasonObjectWithattribute:attribute];
            [array addObject:object];
        }
        
        return [NSArray arrayWithArray:array];
    }
-(id)initSeasonObjectWithattribute:(NSDictionary *)attribute
    {
        self = [super init];
        if (!self) {
            return nil;
        }
        
        self.seasonObjectSeasonId = [[attribute valueForKey:@"seasonId"] integerValue];
        self.seasonObjectSeasonName = [[attribute valueForKey:@"seasonName"] copy];
        
        self.seasonObjectGoalsArray = [GoalsObject arrayGoalsObjectWithAttributes:[attribute valueForKey:@"goals"]];
        
        return self;
    }
    
-(id)copy
    {
        SeasonObject *object = [[SeasonObject alloc] init];
        
        object.seasonObjectSeasonId = self.seasonObjectSeasonId;
        object.seasonObjectSeasonName = [self.seasonObjectSeasonName copy];
        object.seasonObjectGoalsArray = [self.seasonObjectGoalsArray copy];
        
        
        return object;
    }
    
@end

@interface GoalsObject ()
    
    @property  NSInteger goalsObjectPersonId;
    @property  NSInteger goalsObjectCountGoal;
    @property (nonatomic, strong) NSString *goalsObjectPersonName;
    @property NSInteger goalsObjectTeamId;
    @property (nonatomic, strong) NSString *goalsObjectTeamName;

@end

@implementation GoalsObject
    
+(NSArray *)arrayGoalsObjectWithAttributes:(NSArray *)attributes
    {
        NSMutableArray *array = [NSMutableArray array];
        
        for (NSDictionary *attribute in attributes)
        {
            GoalsObject *object = [[GoalsObject alloc] initGoalsObjectWithattribute:attribute];
            [array addObject:object];
        }
        
        return [NSArray arrayWithArray:array];
    }
    
-(id)initGoalsObjectWithattribute:(NSDictionary *)attribute
    {
        self = [super init];
        if (!self) {
            return nil;
        }
        
        self.goalsObjectPersonId = [[attribute valueForKey:@"personId"] integerValue];
        self.goalsObjectCountGoal = [[attribute valueForKey:@"count"] integerValue];
        self.goalsObjectPersonName = [[attribute valueForKey:@"person"] copy];
        self.goalsObjectTeamId = [[attribute valueForKey:@"teamId"] integerValue];
        self.goalsObjectTeamName = [[attribute valueForKey:@"teamName"] copy];
        
        return self;
    }
    
-(id)copy
    {
        GoalsObject *object = [[GoalsObject alloc] init];
        
        object.goalsObjectPersonId = self.goalsObjectPersonId;
        object.goalsObjectCountGoal = self.goalsObjectCountGoal;
        object.goalsObjectPersonName = [self.goalsObjectPersonName copy];
        object.goalsObjectTeamId = self.goalsObjectTeamId;
        object.goalsObjectTeamName = [self.goalsObjectTeamName copy];
        
        return object;
    }

@end



